package tumblr.demo.demotumblr.model;

import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

import tumblr.demo.demotumblr.CGlobal;

/**
 * Created by DELL-PC on 8/30/2016.
 */
public class LoadPhoto extends AsyncTask<String, String, String>{

    @Override
    protected String doInBackground(String... params) {
        try{
            //load photo id
            URL url = new URL("https://www.flickr.com/services/rest/?method=flickr.photos.search&format=json&api_key=8a36a7982aaca5a76f6bdb20aa01ee95&text=phanmem&per_page=40&page=" + new Random().nextInt(5));
            URLConnection connection = url.openConnection();
            DataInputStream dataInputStream = new DataInputStream(connection.getInputStream());
            String strJson = dataInputStream.readLine();
            dataInputStream.close();
            strJson = strJson.substring(14, strJson.length() - 1);
            JSONObject jsonObject = new JSONObject(strJson);
            jsonObject = jsonObject.getJSONObject("photos");
            JSONArray jsonArray = jsonObject.getJSONArray("photo");
            for(int i = 0; i < jsonArray.length(); i++){
                jsonObject = jsonArray.getJSONObject(i);
                Photo photo = new Photo();
                photo.setId(jsonObject.getString("id"));
                CGlobal.myController.addPhotoLoadId(photo);
            }
            //load photo
            Photo photo = null;
            while(CGlobal.myController.getSizeVectorPhotoLoad() > 0){
                photo = null;
                photo = CGlobal.myController.getPhotoLoadId();
                if(photo != null){
                    //load size photo
                    url = new URL("https://www.flickr.com/services/rest/?method=flickr.photos.getSizes&format=json&api_key=8a36a7982aaca5a76f6bdb20aa01ee95&photo_id=" + photo.getId());
                    connection = url.openConnection();
                    dataInputStream = new DataInputStream(connection.getInputStream());
                    strJson = dataInputStream.readLine();
                    dataInputStream.close();
                    strJson = strJson.substring(14, strJson.length() - 1);
                    jsonObject = new JSONObject(strJson);
                    jsonObject = jsonObject.getJSONObject("sizes");
                    jsonArray = jsonObject.getJSONArray("size");
                    jsonObject = jsonArray.getJSONObject(jsonArray.length() < 8? jsonArray.length() - 1 : 7);
                    String source = jsonObject.getString("source");
                    int width = Integer.parseInt(jsonObject.getString("width"));
                    int height = Integer.parseInt((jsonObject.getString("height")));
//                    Log.i("KetQua", "width = " + width + " height = " + height);
                    //load photo
                    if(width > height) {
                        url = new URL(source);
                        connection = url.openConnection();
                        photo.setBitmap(BitmapFactory.decodeStream(connection.getInputStream()));
                        CGlobal.myController.addPhotoUpdatateView(photo);
                    }
                    updateView();
                }
            }
            while(CGlobal.myController.getSizeVectorPhotoUpdateView() >= 4){
                updateView();
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.i("KetQua", e.toString());
        }
        return null;
    }

    private void updateView(){
        if(CGlobal.myController.getSizeVectorPhotoUpdateView() >= 4 && !CGlobal.myController.isLoadSearch()){
            CGlobal.myController.setIsLoadSearch();
            CGlobal.myController.updateView(1);
        }
    }
}
