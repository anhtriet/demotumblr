package tumblr.demo.demotumblr.model;

import android.graphics.Bitmap;

/**
 * Created by DELL-PC on 8/30/2016.
 */
public class Photo {
    String id;
    Bitmap bitmap;

    public void setId(String id){
        this.id = id;
    }
    public void setBitmap(Bitmap bitmap){
        this.bitmap = bitmap;
    }
    public String getId(){
        return id;
    }
    public Bitmap getBitmap(){
        return bitmap;
    }
}
