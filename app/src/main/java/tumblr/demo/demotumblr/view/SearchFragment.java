package tumblr.demo.demotumblr.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import tumblr.demo.demotumblr.CGlobal;
import tumblr.demo.demotumblr.R;

/**
 * Created by DELL-PC on 8/30/2016.
 */
public class SearchFragment extends Fragment {

    View view;
    public SearchFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(view == null){
            view = inflater.inflate(R.layout.search_layout, container, false);
            CGlobal.layoutSearch = (LinearLayout) view.findViewById(R.id.search_layout);
            CGlobal.progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
            CGlobal.progressBar.setVisibility(ProgressBar.VISIBLE);
        }
        return view;
    }
}
