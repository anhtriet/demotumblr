package tumblr.demo.demotumblr.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tumblr.demo.demotumblr.R;

/**
 * Created by DELL-PC on 8/30/2016.
 */
public class AccountFragment extends Fragment {
    public AccountFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.account_layout, container, false);
    }
}
