package tumblr.demo.demotumblr.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import tumblr.demo.demotumblr.view.AccountFragment;
import tumblr.demo.demotumblr.view.HomeFragment;
import tumblr.demo.demotumblr.view.MessageFragment;
import tumblr.demo.demotumblr.view.SearchFragment;

/**
 * Created by DELL-PC on 8/30/2016.
 */
public class PageAdapter extends FragmentStatePagerAdapter {
    public PageAdapter(FragmentManager fm) {
        super(fm);
    }
    Fragment home_fragment = null;
    Fragment search_fragment = null;
    Fragment message_fragment = null;
    Fragment account_fragment = null;

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch(position){
            case 0:
                if(home_fragment == null) home_fragment = new HomeFragment();
                fragment = home_fragment;
                break;
            case 1: if(search_fragment == null) search_fragment = new SearchFragment();
                fragment = search_fragment;
                break;
            case 2: if(message_fragment == null) message_fragment = new MessageFragment();
                fragment = message_fragment;
                break;
            case 3: if(account_fragment == null) account_fragment = new AccountFragment();
                fragment = account_fragment;
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }
//    @Override
//    public CharSequence getPageTitle(int position) {
//        String title = "";
//        switch (position){
//            case 0:
//                title="Home";
//                break;
//            case 1:
//                title="Search";
//                break;
//            case 2:
//                title="Message";
//                break;
//            case 3:
//                title="Account";
//                break;
//        }
//
//        return title;
//    }
}
