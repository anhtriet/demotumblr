package tumblr.demo.demotumblr;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import tumblr.demo.demotumblr.controller.MyController;
import tumblr.demo.demotumblr.model.Photo;
import tumblr.demo.demotumblr.view.PageAdapter;

public class MainActivity extends AppCompatActivity{
    MyHandler handler = new MyHandler();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CGlobal.apps = this;
        setContentView(R.layout.activity_main);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tablayout);
        PageAdapter pageAdapter = new PageAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pageAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabsFromPagerAdapter(pageAdapter);
        tabLayout.getTabAt(1).select();

        //set icon tablayout
        tabLayout.getTabAt(0).setIcon(R.drawable.home);
        tabLayout.getTabAt(0).getIcon().setColorFilter(Color.parseColor("#999999"), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(1).setIcon(R.drawable.search);
        tabLayout.getTabAt(1).getIcon().setColorFilter(Color.parseColor("#EEEEEE"), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(2).setIcon(R.drawable.message);
        tabLayout.getTabAt(2).getIcon().setColorFilter(Color.parseColor("#999999"), PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(3).setIcon(R.drawable.account);
        tabLayout.getTabAt(3).getIcon().setColorFilter(Color.parseColor("#999999"), PorterDuff.Mode.SRC_IN);

        tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager){
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                tab.getIcon().setColorFilter(Color.parseColor("#EEEEEE"), PorterDuff.Mode.SRC_IN);
                tab.select();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
                tab.getIcon().setColorFilter(Color.parseColor("#999999"), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });




        CGlobal.myController = new MyController();
        CGlobal.myController.loadPhoto();
    }

    public boolean updateView(int pView){
        if(handler == null) return false;
        handler.sendMessage(handler.obtainMessage(pView));
        return true;
    }

    class MyHandler extends Handler{

        int step = 0;
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(step == 0){
                step++;
                ImageView imgview = (ImageView) getLayoutInflater().inflate(R.layout.layout_searchview, null, false);
                CGlobal.myController.getLayoutSearch().addView(imgview);
            }
            else if(step == 1){
                step++;
                TextView textView = new TextView(CGlobal.apps);
                textView.setText("TRENDING NOW");
                textView.setTextColor(Color.parseColor("#888888"));
                textView.setTextSize(15);
                CGlobal.myController.getLayoutSearch().addView(textView);
            }
            else if(step == 2){
                step++;
                Photo photo;
                View view = getLayoutInflater().inflate(R.layout.layout_trendingnow, null, false);
                CGlobal.myController.getLayoutSearch().addView(view);
                ImageView img = null;
                for(int i = 0; i < 4; i++){
                    photo = CGlobal.myController.getPhotoUpdateView();
                    if(photo != null){
                        switch(i) {
                            case 0: img = (ImageView) view.findViewById(R.id.imgtrendingnow1);
                                break;
                            case 1: img = (ImageView) view.findViewById(R.id.imgtrendingnow2);
                                break;
                            case 2: img = (ImageView) view.findViewById(R.id.imgtrendingnow3);
                                break;
                            case 3: img = (ImageView) view.findViewById(R.id.imgtrendingnow4);
                                break;
                        }
                        img.setImageBitmap(photo.getBitmap());
                    }
                }
            }
            else if(step == 3){
                step++;
                TextView textView = new TextView(CGlobal.apps);
                textView.setText("RECOMMENDED TUMBLRS");
                textView.setTextColor(Color.parseColor("#888888"));
                textView.setTextSize(15);

                CGlobal.myController.getLayoutSearch().addView(textView);
            }else if(step == 4){
                View view = getLayoutInflater().inflate(R.layout.layout_recommendedtumblrs, null, false);
                Photo photo;
                ImageView img = null;
                for(int i = 0; i < 4; i++){
                    photo = CGlobal.myController.getPhotoUpdateView();
                    if(photo != null){
                        switch(i) {
                            case 0: img = (ImageView) view.findViewById(R.id.imgrecommend1);
                                break;
                            case 1: img = (ImageView) view.findViewById(R.id.imgrecommend2);
                                break;
                            case 2: img = (ImageView) view.findViewById(R.id.imgrecommend3);
                                break;
                            case 3: img = (ImageView) view.findViewById(R.id.imgrecommend4);
                                break;
                        }
                        img.setImageBitmap(photo.getBitmap());
                    }
                }
                TextView tv1 = (TextView) view.findViewById(R.id.textrecomend1);
                TextView tv2 = (TextView) view.findViewById(R.id.textrecomend2);
                String[] str = CGlobal.textTbumlrs.getTextAuto();
                tv1.setText(str[0]);
                tv2.setText(str[1]);
                CGlobal.myController.getLayoutSearch().addView(view);
                view = getLayoutInflater().inflate(R.layout.layout_space, null, false);
                CGlobal.myController.getLayoutSearch().addView(view);
                if(CGlobal.progressBar != null) CGlobal.progressBar.setVisibility(ProgressBar.GONE);
            }
            CGlobal.myController.setIsLoadSearch();
        }
    }
}
