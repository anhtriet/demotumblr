package tumblr.demo.demotumblr.controller;

import android.widget.LinearLayout;

import tumblr.demo.demotumblr.CGlobal;
import tumblr.demo.demotumblr.model.LoadPhoto;
import tumblr.demo.demotumblr.model.Photo;

/**
 * Created by DELL-PC on 8/30/2016.
 */
public class MyController {
    LoadPhoto loadPhoto;

    public MyController(){
        loadPhoto = new LoadPhoto();
    }

    public void loadPhoto(){
        loadPhoto.execute();
    }



    public boolean updateView(int pView){
        if(CGlobal.apps == null) return false;
        if(!CGlobal.apps.updateView(pView)) return false;
        return true;
    }

    public void addPhotoLoadId(Photo photo){
        CGlobal.vPhotoLoad.add(photo);
    }
    public Photo getPhotoLoadId(){
        if(CGlobal.vPhotoLoad.size() > 0){
            Photo photo = CGlobal.vPhotoLoad.elementAt(0);
            CGlobal.vPhotoLoad.remove(0);
            return photo;
        }
        return null;
    }

    public void addPhotoUpdatateView(Photo photo){
        CGlobal.vPhotoUpdateView.add(photo);
    }

    public Photo getPhotoUpdateView(){
        if(CGlobal.vPhotoUpdateView.size() > 0){
            Photo photo = CGlobal.vPhotoUpdateView.elementAt(0);
            CGlobal.vPhotoUpdateView.remove(0);
            return photo;
        }
        return null;
    }

    public int getSizeVectorPhotoUpdateView(){
        return CGlobal.vPhotoUpdateView.size();
    }

    public int getSizeVectorPhotoLoad(){
        return CGlobal.vPhotoLoad.size();
    }

    public LinearLayout getLayoutSearch(){
        return CGlobal.layoutSearch;
    }

    public boolean isLoadSearch(){
        return CGlobal.isLoadSearch;
    }

    public void setIsLoadSearch(){
        CGlobal.isLoadSearch = !CGlobal.isLoadSearch;
    }
}
